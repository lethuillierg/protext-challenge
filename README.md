# README #

A submission to the [ProText challenge (HackerRank)](http://protext.hackerrank.com) by Guillaume Lethuillier.

### What is this repository for? ###

* Quick summary

This program decyphers any sentence encrypted on [this page](http://protext.hackerrank.com). To do so, it extracts the inner structure of the TTF fonts embedded in the page and combines them in order to create a bidirectional map between their respective glyphs. This combination scheme is then used to decypher the text.

The program allows the user to input her/his own text.

The user can also open a temporary rendering of the requested webpage in a browser.

* Version

V. 1.00 - 2017/06/18

### How do I get set up? ###

This is a **Python 3** program that requires the **TTX** tool. The easiest way to install the latter is through the pip command `pip install fonttools`. [More information on the FontTools GitHub repository](https://github.com/fonttools/fonttools).