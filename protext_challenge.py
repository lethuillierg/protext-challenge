'''
Submission to the ProText Challenge <http://protext.hackerrank.com/>

By Guillaume Lethuillier
v. 1.01 | 2017/06/18

Coded in Python 3.6.1

Prerequisite: 
	- TTX <https://github.com/fonttools/fonttools>
	
Tested with:
	- Windows 10 Professional
	- Ubuntu desktop 16.04.2
'''

from lxml import html
from html import escape
import xml.etree.ElementTree as ET
import webbrowser
import requests
import binascii
import tempfile
import shutil
import time
import os
import re


# Make a unique request on the page to solve
base_url = 'http://protext.hackerrank.com/'

print ('Enter a sentence (empty input = default text):')
user_text = escape(input())

if user_text and len(user_text) <= 2048: # 2048: max length of the request (conservative)
	url = base_url + '?text=' + user_text
	print ('\nYour text:', user_text)
else:
	url = base_url

try:
	page = requests.get(url)
	jar = page.cookies
except:
	print ('[!] Connexion error: please ensure that you have access to ', base_url)


# Parse webpage source to get the URLs of the fonts
pattern = re.compile('static[\w\/]+.ttf')
fonts = pattern.findall(str(page.text))

try:
	font_0 = fonts[0] # default font
	font_a = fonts[1]
	font_b = fonts[2]
except:
	print ('[!] Webpage source parsing error: unable to get TTF fonts URLs.')

	
# Get gibberish text from previously requested page
tree = html.fromstring(page.text)
gibberish = tree.xpath('//h2[contains(@id,"gibberish")]/text()')[0]


# Download fonts font_a and font_b
tmp_dir = tempfile.mkdtemp() + '/'
os.makedirs(tmp_dir + 'static/gen/')

def download(font):
	url_font = base_url + font
	dld_font = requests.get(url_font, cookies=jar)
	
	dld_font.raise_for_status()

	with open(tmp_dir + font, 'wb') as file:
			file.write(dld_font.content)
			
	return
	
download(font_a)
download(font_b)


# Convert TTF fonts to XML files with TTX tool
def convert(font):
	ttf_file = tmp_dir + font
	os.system('ttx -q ' + ttf_file)
	
	return

convert(font_a)
convert(font_b)


# Parse XML files to create a map between glyphs' chars and contours
def parse(font):
	# File extension: .ttF -> .ttX
	xml_file = (tmp_dir + font)[:-1] + 'x'
	xml_root = ET.parse(xml_file).getroot()
	
	# Map #1: chars represented by glyphs <=> names of glyphs
	meta_glyphs = xml_root.findall('.//cmap_format_4//')
	cmap = {}
	
	for meta_glyph in meta_glyphs:
		# glyph code -> cast int base 16 -> cast chr -> glyph char
		key = chr(int(meta_glyph.attrib['code'], 16))
		cmap[key] = meta_glyph.attrib['name']
	
	# Map #2: names of glyphs <=> contours of glyphs
	glyphs = xml_root.findall('.//glyf/*')
	glyphmap = {}
	
	for glyph in glyphs:
		key = glyph.attrib['name']
		
		# The value is the content of the XML tag containing contours and instructions casted to string
		value = str(ET.tostring(glyph, encoding='utf-8', method='xml')).replace(key, '')
		
		# A glyph with no contour is equivalent to a space
		if 'contour' not in value:
			value = ' '
			
		glyphmap[key] = value

	# Map #3: chars represented by glyphs <=> contours of glyphs
	map_ = {}
	
	for key, value in cmap.items():
		map_[key] = glyphmap[value]
	
	return map_

map_a = parse(font_a)
map_b = parse(font_b)


# Combine the fonts
def merge(font_a, font_b):
	a_to_b_map = {}
	b_to_a_map = {}
	
	# Build the substitution scheme
	for key_a, value_a in font_a.items():
		for key_b, value_b in font_b.items():
			if value_a == ' ':
				a_to_b_map[key_a] = ' '
				b_to_a_map[' '] = key_a
				break
			elif value_a == value_b:
				a_to_b_map[key_a] = key_b
				b_to_a_map[key_b] = key_a
				break
	
	# Output
	print ('\nSubstitution scheme:')
	for key, value in a_to_b_map.items():
		print (key + ' <=> ' + value + '\t', end='')
	
	print ('\n\nGibberish:', gibberish)
		
	print ('\nDecrypted: ', end='')
	
	for c in gibberish:
		if c in a_to_b_map.keys():
			print (a_to_b_map[c], end='')
		elif c in b_to_a_map.keys():
			print (b_to_a_map[c], end='')
		else:
			# no char is equivalent to a space
			print (' ', end='') 

	print('', flush=True) 
	
	return

merge(map_a, map_b)


# Create and open a local and temporary webpage
def browse():
	download(font_0)

	with open(tmp_dir + 'page.html', 'wb') as file:
				file.write(page.content)
				
	webbrowser.open(tmp_dir + 'page.html', new=0)
	time.sleep(10)
	
	return

print ('\nOpen in browser (default = no): ', end='')
option = input()

if option.lower() == 'yes' or option.lower() == 'y':
	browse()


# Clean
shutil.rmtree(tmp_dir)